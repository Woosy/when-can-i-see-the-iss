# On part d'une image avec OpenJDK + maven 
FROM maven:3.6.3-openjdk-14

# On récupère le code
COPY . .

# On compile l'app et on génère un fichier
# .jar avec les dépendances requises
RUN mvn compile
RUN mvn package
RUN mvn install

# Lors du lancement du conteneur : on lance notre programme
CMD java -jar target/WhenCanISeeTheISS-1.0-SNAPSHOT-jar-with-dependencies.jar
