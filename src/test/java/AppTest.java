import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName ) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite( AppTest.class );
    }

    /**
     * On test que l'on arrive correctement à appeler l'API et qu'elle nous retourne des valeurs
     */
    public void testApp() {
        // TODO: pas eu le temps de rajouter des tests
        // (mais ils s'executent bien sur Gitlab donc pas de soucis à ce niveau)
        assertTrue( true );
    }
}
