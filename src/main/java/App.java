import java.text.DateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import unirest.HttpResponse;
import unirest.JsonNode;
import unirest.Unirest;

public class App {
    private static String host = "http://api.open-notify.org/iss-pass.json";

    public static void main(String[] args) throws Exception {
        
        // Coordonnées indiquées en dur 
        // TODO: récupérer input de l'utilisateur (son adresse)
        // TODO: cabler api google-maps pour récupérer lat & lon à partir de l'adresse
        String lat = "45.759800";
        String lon = "4.785450";

        // Execution de la requête en fonction des coordonnées
        HttpResponse <JsonNode> response = queryIssPass(lat, lon);

        JSONArray passes = response.getBody().getObject().getJSONArray("response");
        JSONObject firstPass = (JSONObject) passes.get(0);
            
        // On récupère & formate la date du prochain passage
        int risetime = firstPass.getInt("risetime");
        Date risetimeDate = new Date((long) risetime * 1000);

        DateFormat fullDateFormat = DateFormat.getDateTimeInstance(
            DateFormat.FULL,
            DateFormat.FULL);

        String date = fullDateFormat.format(risetimeDate);
            
        // On récupère la durée du passage
        int duration = firstPass.getInt("duration") / 60;

        System.out.println("L'ISS va passer le " + date + ", et sera visible pendant " + duration + " minutes...");
    }

    /**
     * Effectue une requête HTTP à l'api qui indique les prochains passages de l'ISS
     * en fonction des coordonnées données
     * @param lat Latitude
     * @param lon Longitude
     * @return HttpResponse <JsonNode>
     */
    public static HttpResponse <JsonNode> queryIssPass(String lat, String lon) {
        // Perform http get request
        HttpResponse <JsonNode> response = Unirest.get(host + "?lat=" + lat + "&lon=" + lon)
            .asJson();
        return response;
    }
}
