# EPSI - PROJET DEVOPS

---

## Groupe

Arthur DUFOUR  
Timothée GRIS  
Alexandre TUET  
Quentin LAPIERRE  
Thomas ZIMMERMMANN  

## Explications et instructions

Au vu des contraintes de temps (instructions pour le projet données un peu tardivement), et du contexte de fin d'année (soutenance titre CDA + préparation et passage MSPR), nous n'avons pas eu le temps de faire une application aussi complète que ce que nous aurions souhaité faire.
Ainsi, notre application se contente d'afficher en ligne de commande, à quelle date et pendant combien de temps la station spatiale internationale (ISS) passe au dessus de Lyon (grâce à une API publique).

Nous n'avons pas eu le temps de réaliser de "vrais" tests pour notre application, nous avons donc pour cela uniquement laissé un "`assertTrue(true)`" pour "simuler" les tests.

Cependant, notre chaîne devops est fonctionnelle :
- lorsque l'on push un commit sur Gitlab, un job lance les tests maven automatiquement
- lorsque l'on push un commit sur la branche master, une image docker embarquant OpenJDK + Maven est build, et est pushée sur le registry publique Gitlab  

Vous pouvez ainsi tester notre application en lancant les deux commandes suivantes :
- `docker pull registry.gitlab.com/woosy/when-can-i-see-the-iss:latest` 
- `docker run registry.gitlab.com/woosy/when-can-i-see-the-iss:latest` 